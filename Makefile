ifndef BISON_PATH
$(error BISON_PATH is not set)
endif
ifndef FLEX_PATH
$(error FLEX_PATH is not set)
endif

B = src/build
FILES = check parse_node absyn_constructors absyn_destroy absyn_node absyn_print preprocessor
HEADERLESS_FILES = kumac.tab kumac.yy main

.PHONY: clean

kumac: $(B)/.sentinel $(FILES:%=$(B)/%.o) $(HEADERLESS_FILES:%=$(B)/%.o)
	gcc $(filter %.o,$^) -o $@

$(B)/.sentinel:
	mkdir -p $(B)
	@touch $@

$(B)/kumac.tab.c $(B)/kumac.tab.h: src/kumac.y
	$(BISON_PATH) --defines=$(B)/kumac.tab.h -o $(B)/kumac.tab.c $<
$(B)/kumac.yy.c $(B)/kumac.yy.h: src/kumac.l
	$(FLEX_PATH) --header-file=$(B)/kumac.yy.h -o $(B)/kumac.yy.c $<

$(B)/kumac.tab.o: $(B)/kumac.tab.c $(B)/kumac.yy.h $(FILES:%=src/%.h)
	gcc -c $< -o $@
$(B)/kumac.yy.o: $(B)/kumac.yy.c $(B)/kumac.tab.h $(FILES:%=src/%.h)
	gcc -c $< -o $@

$(B)/%.o: src/%.c $(FILES:%=src/%.h)
	gcc -c $< -o $@

clean:
	rm -rf $(B)
	rm -f kumac
