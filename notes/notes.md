# General
- dynamically typed
- respects indentation (2 spaces per block layer)
- assignments are expressions

# Syntax
## Valid code
One-line class definition (no contents) is valid.
    class LinkedNode
One-line function definition (no contents) is valid. (Returns nil)
    new()

# Functions
- all functions are expressions
- no function overloading
- default function arguments
- if end of function is reached, returns last expression encountered

# Keywords
- self

# Comments
A comment starts with '#' and continues to the end of the line.
