#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "absyn_constructors.h"
#include "check.h"

int absyn_flatten(void*** array, size_t* length, size_t element_size, const parse_node* node,
                  size_t leaf_length, size_t element_idx, void* extract(const parse_node*)) {
  if (node->length == leaf_length) {
    void** arr = malloc(element_size);
    if (arr == NULL) {
      return -1;
    }
    arr[0] = extract(node->children[0]);
    *array = arr;
    *length = 1;
    return 0;
  }

  // Since we don't know yet the number of elements,
  // for now we allocate memory enough for 4.
  size_t capacity = 4;
  size_t len = 0;

  void** arr = malloc(capacity * element_size);
  if (arr == NULL) {
    return -1;
  }

  while (1) {
    // realloc to fit more elements.
    if (len == capacity) {
      capacity *= 2;
      arr = realloc(arr, capacity * element_size);
      if (arr == NULL) {
        return -1;
      }
    }
    if (node->length == leaf_length) {
      arr[len++] = extract(node->children[0]);
      break;
    }
    arr[len++] = extract(node->children[element_idx]);
    node = node->children[0];
  }

  // Now we have "flattened" all the elements.
  // However, they are in reverse order, so we reverse the array.
  size_t i = 0;
  size_t j = len - 1;
  char* tmp = NULL;
  for (; i < j; ++i, --j) {
    tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
  }

  *array = arr;
  *length = len;
  return 0;
}

absyn_node* absyn_block_constructor(const parse_node* parse_tree) {
  assert(parse_tree->symbol == SYM_BLOCK);
  return new_absyn_tree(parse_tree->children[1]);
}

absyn_node* absyn_class_constructor(const parse_node* parse_tree) {
  assert(parse_tree->symbol == SYM_CLASS);
  absyn_class* class_node = mem_alloc(sizeof(absyn_class));
  absyn_node* result = (absyn_node*) class_node;
  result->children = malloc(sizeof(absyn_node*));
  if (result->children == NULL) {
    free(class_node);
    mem_panic();
  }
  result->children[0] = new_absyn_tree(parse_tree->children[2]);
  result->length = 1;
  result->symbol = ABSYM_CLASS;
  class_node->name = mem_strdup(parse_tree->children[1]->source);
  return result;
}

absyn_node* absyn_expr_constructor(const parse_node* parse_tree) {
  assert(parse_tree->symbol == SYM_EXPR);
  if (parse_tree->length > 1) {
    return new_absyn_tree(parse_tree->children[1]);
  }
  return new_absyn_tree(parse_tree->children[0]);
}

absyn_node* extract_node(const parse_node* node) {
  return new_absyn_tree(node);
}
absyn_node* absyn_exprs_constructor(const parse_node* parse_tree) {
  assert(parse_tree->symbol == SYM_EXPRS);
  absyn_node* result = mem_alloc(sizeof(absyn_node));
  result->symbol = ABSYM_EXPRS;
  if (absyn_flatten((void***) &result->children, &result->length, sizeof(absyn_node*),
                    parse_tree, 2, 1, (void*) extract_node)) {
    free(result);
    mem_panic();
  }
  return result;
}

absyn_node* absyn_func_constructor(const parse_node* parse_tree) {
  assert(parse_tree->symbol == SYM_FUNC);
  absyn_func* func_node = mem_alloc(sizeof(absyn_func));
  absyn_node* result = (absyn_node*) func_node;
  result->symbol = ABSYM_FUNC;
  // NOTE: If there are no params, we only need to allocate for
  // one child, but we allocate two just for simplicity's sake.
  result->children = malloc(2 * sizeof(absyn_node*));
  if (result->children == NULL) {
    free(func_node);
    mem_panic();
  }
  // TODO: Nice processing of params.
  if (parse_tree->length == 5) {
    result->length = 1;
    result->children[0] = new_absyn_tree(parse_tree->children[4]);
  } else {
    result->length = 2;
    result->children[0] = new_absyn_tree(parse_tree->children[3]);
    result->children[1] = new_absyn_tree(parse_tree->children[5]);
  }
  func_node->name = mem_strdup(parse_tree->children[1]->source);
  return result;
}

char* extract_source(const parse_node* node) {
  return mem_strdup(node->source);
}
absyn_node* absyn_params_constructor(const parse_node* parse_tree) {
  assert(parse_tree->symbol == SYM_PARAMS);
  absyn_params* params_node = mem_alloc(sizeof(absyn_params));
  absyn_node* result = (absyn_node*) params_node;
  result->length = 0;
  result->children = NULL;
  result->symbol = ABSYM_PARAMS;

  if (parse_tree->children[0] == T_EMPTY) {
    params_node->ids = NULL;
    params_node->ids_length = 0;
    return result;
  }

  if (absyn_flatten((void***) &params_node->ids, &params_node->ids_length, sizeof(char*),
                    parse_tree, 1, 2, (void*) extract_source)) {
    free(params_node);
    mem_panic();
  }
  return result;
}

absyn_node* absyn_var_constructor(const parse_node* parse_tree) {
  assert(parse_tree->symbol == SYM_VAR);
  absyn_var* var_node = mem_alloc(sizeof(absyn_var));
  absyn_node* result = (absyn_node*) var_node;
  result->length = 0;
  result->children = NULL;
  result->symbol = ABSYM_VAR;

  const parse_node* node = NULL;
  if (parse_tree->length == 3) {
    var_node->of_self = 1;
    node = parse_tree->children[2];
  } else {
    var_node->of_self = 0;
    node = parse_tree->children[0];
  }

  if (absyn_flatten((void***) &var_node->ids, &var_node->ids_length, sizeof(char*),
                    node, 1, 2, (void*) extract_source)) {
    free(var_node);
    mem_panic();
  }
  return result;
}
