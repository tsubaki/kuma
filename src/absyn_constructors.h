#ifndef ABSYN_CONSTRUCTORS_H
#define ABSYN_CONSTRUCTORS_H

#include "absyn_node.h"

absyn_node* absyn_block_constructor(const parse_node* parse_tree);
absyn_node* absyn_class_constructor(const parse_node* parse_tree);
absyn_node* absyn_expr_constructor(const parse_node* parse_tree);
absyn_node* absyn_exprs_constructor(const parse_node* parse_tree);
absyn_node* absyn_func_constructor(const parse_node* parse_tree);
absyn_node* absyn_params_constructor(const parse_node* parse_tree);
absyn_node* absyn_var_constructor(const parse_node* parse_tree);

#endif
