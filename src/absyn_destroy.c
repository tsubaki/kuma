#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "absyn_destroy.h"
#include "check.h"
#include "absyn_print.h"

void absyn_class_destroy(absyn_node* node) {
  assert(node->symbol == ABSYM_CLASS);
  free(((absyn_class*) node)->name);
}

void absyn_func_destroy(absyn_node* node) {
  assert(node->symbol == ABSYM_FUNC);
  free(((absyn_func*) node)->name);
}

void absyn_params_destroy(absyn_node* node) {
  assert(node->symbol == ABSYM_VAR);
  free(((absyn_params*) node)->ids);
}

void absyn_var_destroy(absyn_node* node) {
  assert(node->symbol == ABSYM_VAR);
  free(((absyn_var*) node)->ids);
}

void (*absyn_destructors[ABSYN_NODE_SYMBOL_COUNT])(absyn_node* node) = {
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  absyn_class_destroy,
  NULL,
  NULL,
  absyn_func_destroy,
  NULL,
  NULL,
  absyn_params_destroy,
  absyn_var_destroy,
  NULL,
  NULL
};

void absyn_destroy(absyn_node* node) {
  if (absyn_destructors[node->symbol] != NULL) {
    absyn_destructors[node->symbol](node);
  }
}
