#ifndef ABSYN_DESTROY_H
#define ABSYN_DESTROY_H

#include "absyn_node.h"

void absyn_destroy(absyn_node* node);

#endif
