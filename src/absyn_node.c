#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "check.h"
#include "absyn_node.h"
#include "absyn_constructors.h"
#include "absyn_destroy.h"
#include "absyn_print.h"
#include "parse_node.h"

absyn_node* absyn_filter_constructor(const parse_node* parse_tree) {
  absyn_node* result = mem_alloc(sizeof(absyn_node));

  if (parse_tree->length) {
    size_t stripped_length = 0;
    size_t i;
    for (i = 0; i < parse_tree->length; ++i) {
      if (!parse_tree->children[i]->should_ignore) {
        ++stripped_length;
      }
    }
    assert(stripped_length);

    result->children = malloc(stripped_length * sizeof(absyn_node*));
    if (result->children == NULL) {
      free(result);
      mem_panic();
    }

    result->length = stripped_length;
    result->symbol = (enum absyn_node_symbol) parse_tree->symbol;
    size_t j = 0;
    for (i = 0; i < parse_tree->length; ++i) {
      if (!parse_tree->children[i]->should_ignore) {
        result->children[j++] = new_absyn_tree(parse_tree->children[i]);
      }
    }

    return result;
  }

  result->length = 0;
  result->source = parse_tree->source;
  return result;
}

absyn_node* absyn_leaf_constructor(const parse_node* parse_tree) {
  absyn_node* result = mem_alloc(sizeof(absyn_node));
  result->length = 0;
  result->source = parse_tree->source;
  return result;
}

absyn_node* (*absyn_constructors[PARSE_NODE_SYMBOL_COUNT])(const parse_node* parse_tree) = {
  absyn_filter_constructor,
  absyn_filter_constructor,
  absyn_filter_constructor,
  absyn_block_constructor,
  absyn_filter_constructor,
  absyn_class_constructor,
  absyn_expr_constructor,
  absyn_exprs_constructor,
  absyn_func_constructor,
  absyn_filter_constructor,
  absyn_filter_constructor,
  absyn_params_constructor,
  absyn_var_constructor,
  absyn_filter_constructor,
  absyn_filter_constructor
};

// Builds an abstract syntax tree from a given parse tree.
// Returns a malloc'd absyn_node*.
// TODO: Different constructors for different abstract syntax nodes.
absyn_node* new_absyn_tree(const parse_node* parse_tree) {
  absyn_node* result = NULL;
  if (parse_tree->length) {
    result = absyn_constructors[parse_tree->symbol](parse_tree);
    result->is_proper = 1;
    return result;
  }
  result = absyn_leaf_constructor(parse_tree);
  result->is_proper = 0;
  return result;
}

// Frees the tree starting at a absyn_node.
// FIXME: This recursion can easily exceed some stack size limit.
void free_absyn_node(absyn_node* node) {
  if (node->length) {
    for (size_t i = 0; i < node->length; ++i) {
      free_absyn_node(node->children[i]);
    }
    free(node->children);
    absyn_destroy(node);
  }
  free(node);
}

void escaped_print(const char* source) {
  putchar('"');
  char c;
  for (size_t i = 0; (c = source[i]) != '\0'; ++i) {
    if (c == '\n') {
      printf("\\n");
    } else {
      putchar(c);
    }
  }
  printf("\"\n");
}

// FIXME: This recursion can easily exceed some stack size limit.
void indented_print_tree(absyn_node* node, int indent) {
  for (int i = 0; i < indent; ++i) {
    printf("| ");
  }
  if (node->is_proper) {
    absyn_print(node);
    putchar('\n');
    for (size_t i = 0; i < node->length; ++i) {
      indented_print_tree(node->children[i], indent + 1);
    }
  } else {
    escaped_print(node->source);
  }
}

// Print a human-readable representation of the tree at this node.
void print_tree(absyn_node* node) {
  indented_print_tree(node, 0);
}
