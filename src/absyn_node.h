#ifndef ABSYN_NODE_H
#define ABSYN_NODE_H

#include "parse_node.h"

// Abstract symbol enum mapping.
#define FOREACH_ABSTRACT_SYMBOL(M) M(ARGS), M(ASSIGN), M(BINOP), M(BLOCK), M(CALL), M(CLASS), M(EXPR), M(EXPRS), M(FUNC), M(IF), M(IMM), M(PARAMS), M(VAR), M(VAR2), M(WHILE)
#define ENUM_ABSTRACT_SYMBOL(ABSTRACT_SYMBOL) ABSYM_ ## ABSTRACT_SYMBOL
#define ABSYN_NODE_SYMBOL_COUNT 15
enum absyn_node_symbol { FOREACH_ABSTRACT_SYMBOL(ENUM_ABSTRACT_SYMBOL) };

typedef struct absyn_node {
  size_t length;
  struct absyn_node** children;
  union {
    // FIXME: Is length ever 0? Do we need .source?
    const char* source;
    enum absyn_node_symbol symbol;
  };
  // is_proper flags whether represents a "real" abstract syntax node.
  // FIXME: Is this good design?
  char is_proper;
} absyn_node;

// Builds an abstract syntax tree from a given parse tree.
// Returns a malloc'd absyn_node*.
absyn_node* new_absyn_tree(const parse_node* parse_tree);

// Frees the tree starting at a absyn_node.
// FIXME: This recursion can easily exceed some stack size limit.
void free_absyn_node(absyn_node* node);

// Print a human-readable representation of the tree at this node.
void print_tree(absyn_node* node);

typedef struct absyn_class {
  absyn_node node;
  char* name;
} absyn_class;

typedef struct absyn_func {
  absyn_node node;
  char* name;
} absyn_func;

typedef struct absyn_params {
  absyn_node node;
  char** ids;
  size_t ids_length;
} absyn_params;

typedef struct absyn_var {
  absyn_node node;
  char** ids;
  size_t ids_length;
  char of_self;
} absyn_var;

#endif
