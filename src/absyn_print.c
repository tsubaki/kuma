#include <stdio.h>
#include <assert.h>
#include "absyn_print.h"

#define ABSTRACT_SYMBOL_STRING(ABSTRACT_SYMBOL) #ABSTRACT_SYMBOL
const char* const absyn_node_symbol_strings[ABSYN_NODE_SYMBOL_COUNT] = { FOREACH_ABSTRACT_SYMBOL(ABSTRACT_SYMBOL_STRING) };

void absyn_class_print(absyn_node* node) {
  assert(node->symbol == ABSYM_CLASS);
  printf("%s", ((absyn_class*) node)->name);
}

void absyn_func_print(absyn_node* node) {
  assert(node->symbol == ABSYM_FUNC);
  printf("%s", ((absyn_func*) node)->name);
}

void absyn_params_print(absyn_node* node) {
  assert(node->symbol == ABSYM_PARAMS);
  absyn_params* params_node = (absyn_params*) node;
  if (!params_node->ids_length) {
    printf("()");
    return;
  }
  printf("(%s", params_node->ids[0]);
  for (size_t i = 1; i < params_node->ids_length; ++i) {
    printf(",%s", params_node->ids[i]);
  }
  putchar(')');
}

void absyn_var_print(absyn_node* node) {
  assert(node->symbol == ABSYM_VAR);
  absyn_var* var_node = (absyn_var*) node;
  if (var_node->of_self) {
    printf("self.");
  }
  printf("%s", var_node->ids[0]);
  for (size_t i = 1; i < var_node->ids_length; ++i) {
    printf(".%s", var_node->ids[i]);
  }
}

void (*absyn_printers[ABSYN_NODE_SYMBOL_COUNT])(absyn_node* node) = {
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  absyn_class_print,
  NULL,
  NULL,
  absyn_func_print,
  NULL,
  NULL,
  absyn_params_print,
  absyn_var_print,
  NULL,
  NULL
};

void absyn_print(absyn_node* node) {
  printf("%s ", absyn_node_symbol_strings[node->symbol]);
  if (absyn_printers[node->symbol] != NULL) {
    absyn_printers[node->symbol](node);
  }
}
