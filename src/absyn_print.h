#ifndef ABSYN_PRINT_H
#define ABSYN_PRINT_H

#include "absyn_node.h"

const char* const absyn_node_symbol_strings[ABSYN_NODE_SYMBOL_COUNT];

void absyn_print(absyn_node* node);

#endif
