#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Checks a condition. If it is not met,
// print an error message and terminate the program.
void check(int cond, const char* message) {
  if (!cond) {
    fprintf(stderr, "Error: %s\n", message);
    exit(1);
  }
}

// Checks a condition. If it is not met,
// print a warning message (without terminating the program).
// Returns the result of the condition.
int warn(int cond, const char* message) {
  if (!cond) {
    fprintf(stderr, "Warning: %s\n", message);
  }
  return cond;
}

// Convenience function for panicking on failed malloc.
void mem_panic() {
  fprintf(stderr, "Error: Could not allocate sufficient memory.\n");
  exit(1);
}

#define MEM_CHECK(TYPE, EXPR) TYPE result = EXPR; if (result == NULL) { mem_panic(); } return result

// A malloc that panics on NULL result.
void* mem_alloc(size_t size) {
  MEM_CHECK(void*, malloc(size));
}

// A strdup that panics on NULL result.
char* mem_strdup(const char* s) {
  MEM_CHECK(char*, strdup(s));
}
