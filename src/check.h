#ifndef CHECK_H
#define CHECK_H

// Checks a condition. If it is not met,
// print an error message and terminate the program.
void check(int cond, const char* message);

// Checks a condition. If it is not met,
// print a warning message (without terminating the program).
int warn(int cond, const char* message);

// Convenience function for panicking on failed malloc.
void mem_panic();

// A malloc that panics on NULL result.
void* mem_alloc(size_t size);

// A strdup that panics on NULL result.
char* mem_strdup(const char* s);

#endif
