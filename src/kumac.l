%{
  #include "../parse_node.h"
  #include "kumac.tab.h"
  #include "../check.h"

  int action(int token_code, const parse_node* node, size_t length);
  void skip(size_t length);
  void newline();

  #define KW_ACTION(TOKEN, LENGTH) return action(KW_ ## TOKEN, T_ ## TOKEN, LENGTH)
  #define CHAR_ACTION(CHAR, NODE) return action(CHAR, NODE, 1)
%}

digit   [0-9]
letter  [a-zA-Z_]

%%
"class"                     { KW_ACTION(CLASS, 5); }
"else"                      { KW_ACTION(ELSE, 4); }
"fn"                        { KW_ACTION(FN, 2); }
"if"                        { KW_ACTION(IF, 2); }
"nil"                       { KW_ACTION(NIL, 3); }
"self"                      { KW_ACTION(SELF, 4); }
"while"                     { KW_ACTION(WHILE, 5); }
{letter}({letter}|{digit})* { return action(ID, new_token(mem_strdup(yytext)), strlen(yytext)); }
{digit}+                    { return action(INT, new_token(mem_strdup(yytext)), strlen(yytext)); }
"\n"                        { newline(); CHAR_ACTION('\n', T_NL); }
"\\"                        { newline(); }
"("                         { CHAR_ACTION('(', T_LP); }
")"                         { CHAR_ACTION(')', T_RP); }
"{"                         { newline(); yylval = T_LCB; return '{'; }
"}"                         { CHAR_ACTION('}', T_RCB); }
"["                         { CHAR_ACTION('[', T_LSB); }
"]"                         { CHAR_ACTION(']', T_RSB); }
","                         { CHAR_ACTION(',', T_COM); }
"."                         { CHAR_ACTION('.', T_DOT); }
"+"                         { CHAR_ACTION('+', T_PLUS); }
"-"                         { CHAR_ACTION('-', T_MINUS); }
"*"                         { CHAR_ACTION('*', T_STAR); }
"/"                         { CHAR_ACTION('/', T_SLASH); }
"="                         { CHAR_ACTION('=', T_EQ); }
[ \t\r]                     { skip(1); }
.                           { CHAR_ACTION(yytext[0], NULL); }
%%

int yywrap() {
  return 1;
}

int action(int token_code, const parse_node* node, size_t length) {
  yylval = node;
  yylloc.first_column = yylloc.last_column + 1;
  yylloc.last_column += length;
  return token_code;
}

void skip(size_t length) {
  yylloc.first_column = yylloc.last_column + 1;
  yylloc.last_column += length;
}

void newline() {
  ++yylloc.first_line;
  ++yylloc.last_line;
  yylloc.first_column = 0;
  yylloc.last_column = 0;
}
