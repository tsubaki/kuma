%{
  #define YYDEBUG 1

  #include <stdio.h>
  #include <assert.h>
  #include "kumac.yy.h"
  #include "../parse_node.h"
  #include "../preprocessor.h"

  void yyerror(const parse_node** parse_tree, const char* source, const char* message);

  #define EMPTY(SYMBOL) yyval = new_nonterminal(ENUM_SYMBOL(SYMBOL), 1, &T_EMPTY)
  #define NODE(SYMBOL, LENGTH) yyval = new_nonterminal(ENUM_SYMBOL(SYMBOL), LENGTH, yyvsp - LENGTH + 1)
%}

%define api.value.type {const parse_node*}
%parse-param {const parse_node** parse_tree} {const char* source}
%locations

%token ID
%token INT
%token KW_CLASS
%token KW_ELSE
%token KW_FN
%token KW_NIL
%token KW_SELF
%token KW_WHILE

%precedence KW_IF
%precedence '\n'
%precedence '='
%left '+' '-'
%left '*' '/'
%precedence '('

%%
program:
  exprs                               { *parse_tree = $1; }
;

exprs:
  expr '\n'                           { NODE(EXPRS, 2); }
| exprs expr '\n'                     { NODE(EXPRS, 3); }
;

expr:
  assign                              { NODE(EXPR, 1); }
| binop                               { NODE(EXPR, 1); }
| call                                { NODE(EXPR, 1); }
| class                               { NODE(EXPR, 1); }
| func                                { NODE(EXPR, 1); }
| if                                  { NODE(EXPR, 1); }
| imm                                 { NODE(EXPR, 1); }
| var                                 { NODE(EXPR, 1); }
| while                               { NODE(EXPR, 1); }
| '(' expr ')'                        { NODE(EXPR, 3); }
;

assign:
  var '=' expr                        { NODE(ASSIGN, 3); }
;

binop:
  expr '+' expr                       { NODE(BINOP, 3); }
| expr '-' expr                       { NODE(BINOP, 3); }
| expr '*' expr                       { NODE(BINOP, 3); }
| expr '/' expr                       { NODE(BINOP, 3); }
;

call:
  expr '(' args ')'                   { NODE(CALL, 4); }
;

class:
  KW_CLASS ID block                   { NODE(CLASS, 3); }
;

func:
  KW_FN ID '(' ')' block              { NODE(FUNC, 5); }
| KW_FN ID '(' params ')' block       { NODE(FUNC, 6); }
;

if:
  KW_IF expr block                    { NODE(IF, 3); }
| KW_IF expr block '\n' KW_ELSE block { NODE(IF, 6); }
;

imm:
  INT                                 { NODE(IMM, 1); }
| KW_NIL                              { NODE(IMM, 1); }
| KW_SELF                             { NODE(IMM, 1); }
;

var:
  var2                                { NODE(VAR, 1); }
| KW_SELF '.' var2                    { NODE(VAR, 3); }
;

var2:
  ID                                  { NODE(VAR2, 1); }
| var2 '.' ID                         { NODE(VAR2, 3); }
;

while:
  KW_WHILE expr block                 { NODE(WHILE, 3); }
;

args:
  %empty                              { EMPTY(ARGS); }
| expr                                { NODE(ARGS, 1); }
| args ',' expr                       { NODE(ARGS, 3); }
;

block:
  '{' exprs '}'                       { NODE(BLOCK, 3); }
;

params:
  ID                                  { NODE(PARAMS, 1); }
| params ',' ID                       { NODE(PARAMS, 3); }
;
%%


void yyerror(const parse_node** parse_tree, const char* source, const char* message) {
  assert(yylloc.first_line == yylloc.last_line);
  size_t i = line_index(source, yylloc.last_line - 1);
  size_t line_start = i;
  for (; source[i] == ' '; ++i) {}
  size_t spaces = i - line_start;
  fprintf(stderr, "%s: [%d:%lu-%lu] ", message, yylloc.last_line,
          spaces + yylloc.first_column, spaces + yylloc.last_column);
  for (; source[i] != '\n' && source[i] != '\\' && source[i] != '\0'; ++i) {
    putc(source[i], stderr);
  }
  putc('\n', stderr);
}
