#define YYDEBUG 1

#include <stdio.h>
#include <string.h>
#include "check.h"
#include "preprocessor.h"
#include "parse_node.h"
#include "absyn_node.h"
#include "build/kumac.tab.h"
#include "build/kumac.yy.h"

static const char* USAGE = "Usage: kumac [-d] <filename>\n  -d  Show Bison debug output.";

int main(int argc, char const* argv[]) {
  const char* filename;

  if (argc != 2) {
    if (argc == 3 && strcmp(argv[1], "-d") == 0) {
      yydebug = 1;
      filename = argv[2];
    } else {
      fprintf(stderr, "%s\n", USAGE);
      return 1;
    }
  } else {
    filename = argv[1];
  }

  size_t filesize;
  char* source = read_file(filename, &filesize);

  yylloc.first_line = yylloc.last_line = 1;
  yylloc.first_column = yylloc.last_column = 0;

  char* input = preprocess(source, filesize);
  if (input[0] == '\0') {
    free(input);
    return 0;
  }

  YY_BUFFER_STATE buffer = yy_scan_string(input);
  free(input);

  const parse_node* parse_tree = NULL;
  check(!yyparse(&parse_tree, source), "Could not parse file.");
  yy_delete_buffer(buffer);
  free(source);

  // DEBUG
  // print_parse_tree(parse_tree);

  absyn_node* ast = new_absyn_tree(parse_tree);
  free_parse_node(parse_tree);

  print_tree(ast);

  free_absyn_node(ast);

  return 0;
}
