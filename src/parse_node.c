#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "check.h"
#include "parse_node.h"

#define TERMINAL(ID, IGNORE, SOURCE) const parse_node T_STRUCT_ ## ID = { 0, IGNORE, 0, SOURCE }; const parse_node* const T_ ## ID = &T_STRUCT_ ## ID

TERMINAL(EMPTY, 1, "");

TERMINAL(NL, 1, "\n");
TERMINAL(LP, 1, "(");
TERMINAL(RP, 1, ")");
TERMINAL(LCB, 1, "{");
TERMINAL(RCB, 1, "}");
TERMINAL(LSB, 1, "[");
TERMINAL(RSB, 1, "]");
TERMINAL(COM, 1, ",");
TERMINAL(DOT, 1, ".");
TERMINAL(PLUS, 0, "+");
TERMINAL(MINUS, 0, "-");
TERMINAL(STAR, 0, "*");
TERMINAL(SLASH, 0, "/");
TERMINAL(EQ, 1, "=");

TERMINAL(CLASS, 1, "class");
TERMINAL(ELSE, 1, "else");
TERMINAL(FN, 1, "fn");
TERMINAL(IF, 1, "if");
TERMINAL(NIL, 0, "nil");
TERMINAL(SELF, 0, "self");
TERMINAL(WHILE, 1, "while");

#define SYMBOL_STRING(SYMBOL) #SYMBOL
const char* const parse_node_symbol_strings[PARSE_NODE_SYMBOL_COUNT] = { FOREACH_SYMBOL(SYMBOL_STRING) };

// Initializes a new `symbol` node with `length` children.
const parse_node* new_nonterminal(enum parse_node_symbol symbol, size_t length, const parse_node* const* children) {
  assert(length);

  parse_node* result = mem_alloc(sizeof(parse_node));
  result->children = malloc(length * sizeof(parse_node*));
  if (result->children == NULL) {
    free(result);
    mem_panic();
  }

  result->length = length;
  result->should_ignore = children[0] == T_EMPTY;
  result->symbol = symbol;
  for (size_t i = 0; i < length; ++i) {
    result->children[i] = children[i];
  }

  return result;
}

// Initializes a new token node representing `source`.
// Note: If the token is a constant, use the declared T_X
// instead of initializing a new node with this function.
const parse_node* new_token(const char* source) {
  parse_node* result = mem_alloc(sizeof(parse_node));
  result->length = 0;
  result->should_ignore = 0;
  result->is_allocated = 1;
  result->source = source;
  return result;
}

// Frees the tree starting at a parse_node.
// Does nothing if the node was statically initialized.
// FIXME: This recursion can easily exceed some stack size limit.
void free_parse_node(const parse_node* node) {
  if (node->length) {
    for (size_t i = 0; i < node->length; ++i) {
      free_parse_node(node->children[i]);
    }
    free(node->children);
    free((parse_node*) node);
  } else if (node->is_allocated) {
    free((char*) node->source);
    free((parse_node*) node);
  }
}

void parse_node_escaped_print(const char* source) {
  putchar('"');
  char c;
  for (size_t i = 0; (c = source[i]) != '\0'; ++i) {
    if (c == '\n') {
      printf("\\n");
    } else {
      putchar(c);
    }
  }
  printf("\"\n");
}

// FIXME: This recursion can easily exceed some stack size limit.
void indented_print_parse_tree(const parse_node* node, int indent) {
  for (int i = 0; i < indent; ++i) {
    printf("| ");
  }
  if (node->length) {
    if (node->length == 1 && !node->children[0]->length) {
      printf("%s ", parse_node_symbol_strings[node->symbol]);
      parse_node_escaped_print(node->children[0]->source);
    } else {
      printf("%s\n", parse_node_symbol_strings[node->symbol]);
      for (size_t i = 0; i < node->length; ++i) {
        indented_print_parse_tree(node->children[i], indent + 1);
      }
    }
  } else {
    parse_node_escaped_print(node->source);
  }
}

// Print a human-readable representation of the tree at this node.
void print_parse_tree(const parse_node* node) {
  indented_print_parse_tree(node, 0);
}
