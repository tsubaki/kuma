#ifndef PARSE_NODE_H
#define PARSE_NODE_H

// Symbol enum mapping.
#define FOREACH_SYMBOL(M) M(ARGS), M(ASSIGN), M(BINOP), M(BLOCK), M(CALL), M(CLASS), M(EXPR), M(EXPRS), M(FUNC), M(IF), M(IMM), M(PARAMS), M(VAR), M(VAR2), M(WHILE)
#define ENUM_SYMBOL(SYMBOL) SYM_ ## SYMBOL
#define PARSE_NODE_SYMBOL_COUNT 15
enum parse_node_symbol { FOREACH_SYMBOL(ENUM_SYMBOL) };

typedef struct parse_node {
  size_t length;
  char should_ignore;
  union {
    char is_allocated;
    const struct parse_node** children;
  };
  union {
    const char* source;
    enum parse_node_symbol symbol;
  };
} parse_node;

// Constant token nodes.
const parse_node* const T_EMPTY;

const parse_node* const T_NL;
const parse_node* const T_LP;
const parse_node* const T_RP;
const parse_node* const T_LCB;
const parse_node* const T_RCB;
const parse_node* const T_LSB;
const parse_node* const T_RSB;
const parse_node* const T_COM;
const parse_node* const T_DOT;
const parse_node* const T_PLUS;
const parse_node* const T_MINUS;
const parse_node* const T_STAR;
const parse_node* const T_SLASH;
const parse_node* const T_EQ;

const parse_node* const T_CLASS;
const parse_node* const T_ELSE;
const parse_node* const T_FN;
const parse_node* const T_IF;
const parse_node* const T_NIL;
const parse_node* const T_SELF;
const parse_node* const T_WHILE;

// Initializes a new `symbol` node with `length` children.
const parse_node* new_nonterminal(enum parse_node_symbol symbol, size_t length, const parse_node* const* children);

// Initializes a new token node representing `source`.
// Note: If the token is a constant, use the declared T_X
// instead of initializing a new node with this function.
const parse_node* new_token(const char* source);

// Frees the tree starting at a parse_node.
// Does nothing if the node was statically initialized.
void free_parse_node(const parse_node* node);

// Print a human-readable representation of the tree at this node.
void print_parse_tree(const parse_node* node);

#endif
