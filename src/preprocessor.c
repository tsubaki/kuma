// Kuma preprocessor
//
// Parses block boundaries by indentation and inserts {}.

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "check.h"

static const int INDENT_SIZE = 2;
static const int AT_EOF = -1;
static const int EMPTY_LINE = -2;

// Reads a file and returns a string of its contents.
// Stores the size of the file in `size`.
char* read_file(const char* filename, size_t* size) {
  FILE* file = fopen(filename, "r");
  check(file != NULL, "Could not open file.");

  struct stat st;
  check(!stat(filename, &st), "Could not determine file size.");

  char* result = mem_alloc(st.st_size + 1);

  size_t i = 0;
  while (i < st.st_size) {
    result[i++] = fgetc(file);
  }
  result[i] = '\0';

  fclose(file);  // Ignore failure EOF return value.
  *size = st.st_size;
  return result;
}

// Returns the number of indents on the current line.
// An indent is INDENT_SIZE number of spaces.
// Assumes that `index` is at a line start.
int get_indent(const char* source, size_t* index) {
  size_t start_index = *index;
  for (;source[*index] == ' '; ++*index) {}

  char c = source[*index];
  // Now `c` is the first non-space character on the line.
  // TODO: Handle comments.

  if (c == '\n') {
    // The entire line contains only spaces.
    return EMPTY_LINE;
  }
  if (c == '\0') {
    return AT_EOF;
  }

  size_t spaces = *index - start_index;

  // The indent must be a multiple of INDENT_SIZE.
  check(spaces % INDENT_SIZE == 0, "Unexpected indentation");
  return spaces / INDENT_SIZE;
}

// Parses block boundaries by indentation, inserting "{" and "}\n" instead.
// Returns a malloc'd result string.
char* preprocess(const char* source, size_t size) {
  // Since INDENT_SIZE = 2, st.st_size + 2 is enough space.
  // The extra 2 are for a missing '\n' at EOF and '\0'.
  char* result = mem_alloc(size + 2);

  size_t i = 0;
  size_t src_i = 0;
  int indent = 0;
  int next_indent;
  int is_empty = 1;
  
  while ((next_indent = get_indent(source, &src_i)) != AT_EOF) {
    // `next_indent` is the number of indents on this line.
    if (next_indent == EMPTY_LINE) {
      result[i++] = '\\';
      ++src_i;
      continue;
    }

    if (next_indent != indent) {
      // First non-blank line cannot be indented.
      check(!is_empty, "Unexpected indentation.");

      if (next_indent == indent + 1) {
        // Deeper block boundary.
        result[i - 1] = '{';
        indent = next_indent;
      } else if (next_indent < indent) {
        // Exit inner blocks.
        for (; indent > next_indent; --indent) {
          result[i++] = '}';
          result[i++] = '\n';
        }
      } else {
        // Indenting deeper by more than one level is not allowed.
        // We panic.
        check(0, "Unexpected indentation.");
      }
    }

    is_empty = 0;

    // Copy the rest of the line.
    for (; (result[i] = source[src_i]) != '\n' && result[i] != '\0'; ++i, ++src_i) {}
    if (result[i] == '\n') {
      ++i;
      ++src_i;
    } else {
      // EOF.
      break;
    }
  }

  if (is_empty) {
    // Empty file.
    result[0] = '\0';
    return result;
  }

  if (result[i - 1] != '\n') {
    // Add missing '\n' to end the last expression.
    result[i++] = '\n';
  }

  for (; indent > 0; --indent) {
    // Close remaining blocks.
    result[i++] = '}';
    result[i++] = '\n';
  }

  result[i] = '\0';
  return result;
}

// Returns the index of the nth line in a source code string.
size_t line_index(char* source, size_t n) {
  size_t index = 0;
  size_t line_number = 0;
  for (; line_number != n && source[index] != '\0'; ++index) {
    if (source[index] == '\n') {
      ++line_number;
    }
  }
  return index;
}
