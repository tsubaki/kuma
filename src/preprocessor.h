#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H

// Reads a file and returns a string of its contents.
// Stores the size of the file in `size`.
char* read_file(const char* filename, size_t* size);

// Parses block boundaries by indentation, inserting "{" and "}\n" instead.
// Returns a malloc'd result string.
char* preprocess(const char* source, size_t size);

// Returns the index of the nth line in a source code string.
size_t line_index(const char* source, size_t n);

#endif
